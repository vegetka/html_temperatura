Weather station located in Beechwood Court, Cobh, Ireland

Parts: 
- core: ATmega328 
- wifi: ESP8266
- sensors: 
        - DHT11 - temperature and humidity
        - DS18B20 - temperature

Chip software: own.
Website code: own.
Database: thingspeak.com